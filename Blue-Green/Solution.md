# Mise en oeuvre du principe Blue-Green

- Suppressions applications et les routes associées (si elles existent)
```sh
cf delete -f -r MyApp-Prod
cf delete -f -r MyApp-Beta
cf push
```

- Vérification:
```sh
cf routes
development   rtgr-app                           cfapps.io                        Blue
development   rtgr-app-temp                      cfapps.io                        Green
```

- On veut maintenant que MyApp-Beta (nouvelle version de l'appli) soit accessible via la route standard de l'application, à savoir prod-rtgr-1.cfapps.io
```sh
cf map-route MyApp-Beta cfapps.io -n prod-rtgr-1
```

- Le résultat:
```
cf routes
development   prod-rtgr-1   cfapps.io                        MyApp-Prod,MyApp-Beta
development   beta-rtgr-1   cfapps.io                        MyApp-Beta
```
> L'adresse rtgr-app.cfapps.io permet donc maintenant d'atteindre les deux versions de l'application. C'est le routeur qui décide... Pas facile à tester à cause du sticky session... On peut néamoins y arriver en passant de WIFI à 3G (nouvelle IP + vidage cookies)

- On peut ensuite supprimer le lien entre la route de "prod" rtgr-app.cfapps.io et l'ancienne (et donc désuette) version de l'application
```sh
cf unmap-route MyApp-Prod cfapps.io -n prod-rtgr-1
cf delete -f -r MyApp-Prod
cf rename  MyApp-Beta MyApp-Prod
cf delete-route -f cfapps.io --hostname beta-rtgr-1
```


- Conclusion
```sh
cf routes
development   prod-rtgr-1   cfapps.io                        MyApp-Beta
development   beta-rtgr-1   cfapps.io                        MyApp-Beta
```

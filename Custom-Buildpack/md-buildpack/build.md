# Construction du buildpack custom

* on positionne la bonne version de ruby 
```
rbenv shell 2.2.3
```

* On crée le zip à envoyer dans Cloud Foundry
```
BUNDLE_GEMFILE=cf.Gemfile bundle exec buildpack-packager --cached
```

* On pousse le zip en lui associant un nom logique et un ordre de priorité
```
cf create-buildpack md_static_buildpack markdown-staticfile_buildpack-cached-v1.0.0.zip 1
```

* Si le buildpack existe déja, il faut le mettre à jour
```
cf  update-buildpack md_static_buildpack -p ./markdown-staticfile_buildpack-cached-v1.0.0.zip  -i 0
```

* Ou le supprimer
```
cf delete-buildpack -f md_static_buildpack
```


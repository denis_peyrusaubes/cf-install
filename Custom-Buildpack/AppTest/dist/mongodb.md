Durée: 	3 jours
Réf.: 	MGOBEG
URL:    [https://www.retengr.com/formation-developper-avec-mongodb](/formation-developper-avec-mongodb)
Infos:  [info@retengr.com](mailto:info@retengr.com)


##Présentation

On ne présente plus MongoDB. Couramment utilisé, le stockage en mode document est une composante importante des systèmes d’informations à forte implications internet. Utilisé par de grands acteurs tels que eBay, FourSquare, SourceForge, MongoDB a su séduire en apportant des solutions innovantes aux problèmes que les bases de données relationnelles ne parvenaient pas à résoudre, le tout en s’exécutant sur des infrastructures peu couteuses, hébergées chez soi, ou dans le cloud.
##Objectifs
Cette formation vous fera découvrir le mode de fonctionnement d’un système de gestion de données de type documents. CRUDs, validation de données, requêtes pour développer, mais aussi gestion de la montée en charge ainsi que de la tolérance pour gérer la production. La pratique (75%) sera préférée à la théorie (25%) afin de comprendre et mettre en oeuvre tous ces concepts.

##Audience 
Architectes, Développeurs et chefs de projets techniques


##Prérequis 
La connaissance  d’un langage de programmation structuré  ainsi que de JSON est recommandé, mais n’est pas impératif.

##Méthode pédagogique

75% de travaux pratiques

##Programme

######Présentation
Not only SQL
Architecture
Moteurs de stockage
Les drivers

######Modèle de données
Un document: l’élément de base
Structure d’une collection
Relation et NoSQL ?
Référence et documents embarqués
Quid de l’intégrité ?

######Les CRUDs
Insertion
Insertion en masse
Mise à jour
Suppression
Traitement de masse (Bulk)

######Agrégation
Pipeline
Agrégation simple
Les différents opérateurs

######Index 
Utilité ?
simple | composé
Index typés: géospatial, texte, …
Propriété d’index
Analyse Query performance

######Sharding & Replica
Failover | load balancing
répartition
Volume de données 
Administration







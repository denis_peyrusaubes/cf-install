Durée: 	5 jours
Réf.: 	IOSBEGSFT
URL:    [https://www.retengr.com/formation-developper-sur-ios-avec-swift](/formation-developper-sur-ios-avec-swift)
Infos:  [info@retengr.com](mailto:info@retengr.com)

Si vous déjà expérimenté sur iOS avec ObjectiveC, la formation [SWIFT2](/developper-avec-swift) sera certainement plus adaptée à vos besoins.

##Présentation
Apple ne se doutait certainement pas que la création de l’iPhone OS en 2008, serait à l’origine d’une telle révolution dans le domaine de la mobilité. Renommé quelques années plus tard en iOS, le système d’exploitation équipant aujourd’hui iPhone, iPad, et iPod Touch, est devenu un acteur incontournable dans l’évolution de la mobilité aujourd’hui. 
Basé sur les environnements de développement traditionnel chez Apple (MacOS & Xcode), la montée en compétence sur cette plateforme peut s’avérer délicat pour ceux qui n’ont jamais eu l’occasion de se frotter aux produits à la pomme.

##Objectifs
A l’issu de cette formation, vous pourrez développer une application iOS (iPhone ou iPad) utilisant en utilisant le langage Swift 2 et l’ensemble des composants graphiques mis à disposition par le framework UIKIT, vous pourrez aussi communiquer avec diverses sources de données: webservices REST, base de données locale via le framework COREDATA, lancer des traitement asynchrone, utiliser l’ensemble des capteurs à disposition (géolocalisation, accéléromètre). 
Au fur et à mesure de la formation, vous bénéficierez des conseils avisés du formateurs afin de prendre rapidement en mains un système d’exploitation (MacOS) qui est peut-être nouveau pour vous, ainsi que son environnement de développement associé (Xcode).

##Audience
Développeurs et chefs de projets techniques

##Prérequis 
Afin de suivre cette formation dans de bonnes conditions, il est nécessaire d’avoir pratiqué la programmation à l’aide  d’un langage de structuré (C/C++/Java/C#).

##Méthode pédagogique
70% de travaux pratiques

##Programme
######iOS & iDevices : caractéristiques générales
Processeur / Mémoire
Connectivité / Capteurs
Modèle commercial

######SWIFT2
Créer une classe
Les bases objets: Héritage / Protocole
Les classes de base: Collections / Chaîne de caractère
Les structures de contrôles
Les types optionnels
Chaînage d'optionnels

######Gestion mémoire
La gestion de la mémoire en SWIFT: beaucoup de transparence, mais...
Principe de base: le comptage de références
Autorelease Pool
ARC : Automatic Reference Counting
Optimisation et analyse de la mémoire (Code Coverage, Leaks, Object Allocation, Zombie)
######Les règles d'ergonomie
Un document de référence: Human Interface Guidelines

######Interface Builder
Présentation
Les storyboards
Les Segues 

######Les composants graphiques
Le composant de base: UIView
L’ensemble des composants du framework UIKIT: UITableView / …

######La gestion des événements 
Evénement bas niveau: Multitouch
le modèle target/action 
IBAction

######Les contrôleurs
UIViewController
UITableViewController
UICollectionViewController
UINavigationController

######Gestion des entrées / sorties
Le système de fichier
Les couches réseaux TCPIP / HTTP
Webservices REST et SOAP
Base de données SQLite / CoreData
Introduction à iCloud

######Quelques frameworks iOS
CoreLocation (GPS)
CoreMotion (accéléromètre)
AVFoundation (Photo / vidéos)
AirPrint architecture

######Déploiement dans l’AppStore
Modèle économique
Déployer sur un iDevice pour tester
Certificat / Provisioning profile
Déploiement entreprise / OTA
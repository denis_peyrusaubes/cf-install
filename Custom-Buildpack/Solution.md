# Construction d'un buildpack custom


1. on positionne la bonne version de ruby
```sh
rbenv shell 2.2.3
```
> Cette opération n'est peut-être pas la même suivant votre environnement de développement. Nous utilisons ici l'utilitaire _rbenv_ qui permet de passer d'une version de ruby à une autre assez simplement.


1. On crée le zip à envoyer dans Cloud Foundry
```sh
BUNDLE_GEMFILE=cf.Gemfile bundle exec buildpack-packager --cached
```

1. On pousse le zip en lui associant un nom logique et un ordre de priorité
```sh
cf create-buildpack md_static_buildpack markdown-staticfile_buildpack-cached-v1.0.0.zip 1
```

1. Si le buildpack existe déja, il faut le mettre à jour
```sh
cf  update-buildpack md_static_buildpack -p ./markdown-staticfile_buildpack-cached-v1.0.0.zip  -i 0
```

1. Ou le supprimer
```sh
cf delete-buildpack -f md_static_buildpack
```


# FICHE INSTALL FORMATION CLOUDFOUNDRY


## Remarques
Je n’ai pas encore eu la possibilité de tester la configuration décrite dans ce document, à cela plusieurs raisons:

- L’outil principal qui est utilisé dans cette version « Pivotal Cloud Foundry Dev » PCFDev a sorti trois nouvelles versions depuis la session pilote du mois de juin dernier (de 0.15 à 0.18).
- Souhaitant utiliser la dernière version, une requalification de la formation sur la version 0.18 est planifié la première semaine semaine du mois d’août

## Elements de base

|  	| 	|
|--------------	|---------------------------------------------------------	|
| Distribution 	| Ubuntu 14.04 	|
| Droit 	| Compte administrateur (root) 	|
| Réseau 	| Réseau « livebox » indépendant du réseau interne Orange 	|
| Disque dur 	| Un minimum de 50Go devra être disponible sur le disque 	|
| RAM 	| 8 Go est un minimum, 16 Go est recommandé 	|

## Produits à installer

- [Virtual Box](https://doc.ubuntu-fr.org/virtualbox)
- [Vagrant](https://www.vagrantup.com/downloads.html) (Merci de l’installer même si l’utilisation de PCFDev 0.18 ne le rend plus nécessaire, cela permettra de redescendre sur la version PCDDev 0.15 en cas de problème)
- [Cloudfoundry CLI](https://github.com/cloudfoundry/cli#downloads)
- *rbenv*: [tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-ubuntu-14-04). Attention: il n'est pas nécessaire d'installer Rail. Une fois *rbenv* installé::

    - ```rbenv install ruby 2.2.3```
    - ```rbenv shell 2.2.3```
    - ```gem install bundler```


- *JDK 1.8*
- *Maven*
- *Eclipse*
- *PCFDev V0.18* (disponible sur le site de pivotal, dans la section network, après avoir créé un compte et s’être identifié)
- *buildpack-packager*: son installation se fera automatiquemenet lors de la vérification 2 décrite ci-dessous.

## Vérification 1: Blue-Green

```
cd Blue-Green
cf push
cf routes
  development   rtgr-app                           cfapps.io                        Blue
  development   rtgr-app-temp                      cfapps.io                        Green
```

## Vérification 2: Custom Buildpack

```
cd Custom-Buildpack
rbenv shell 2.2.3
BUNDLE_GEMFILE=cf.Gemfile bundle install
BUNDLE_GEMFILE=cf.Gemfile bundle exec buildpack-packager --cached
cf create-buildpack md_static_buildpack markdown-staticfile_buildpack-cached-v1.0.0.zip 1
```
